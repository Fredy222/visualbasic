﻿Public Class Direccion
    Private direccionID As Integer
    Public Property ID() As UInteger
        Get
            Return direccionID
        End Get
        Set(value As UInteger)
            direccionID = value
        End Set
    End Property
    Private direccioneAddress As String
    Public Property Direccion() As String
        Get
            Return direccioneAddress
        End Get
        Set(value As String)
            direccioneAddress = value
        End Set
    End Property
    Private direccionCiudad As String
    Public Property Ciudad() As String
        Get
            Return direccionCiudad
        End Get
        Set(value As String)
            direccionCiudad = value
        End Set
    End Property
    Private direccionEstado As String
    Public Property Estado() As String
        Get
            Return direccionEstado

        End Get
        Set(value As String)
            direccionEstado = value
        End Set
    End Property
    Private direccionTelefono As String
    Public Property Telefono() As String
        Get
            Return direccionTelefono
        End Get
        Set(value As String)
            direccionTelefono = value
        End Set
    End Property
    Private direccionCelular As String
    Public Property Celular() As String
        Get
            Return direccionCelular
        End Get
        Set(value As String)
            direccionCelular = value
        End Set
    End Property
    Private direccionRif As String
    Public Property Rif() As String
        Get
            Return direccionRif
        End Get
        Set(value As String)
            direccionRif = value
        End Set
    End Property
    Private direccionPuntoReferncia As String
    Public Property PuntoReferencia() As String
        Get
            Return direccionPuntoReferncia
        End Get
        Set(value As String)
            direccionPuntoReferncia = value
        End Set
    End Property
End Class
