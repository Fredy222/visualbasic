﻿Public Class Cliente
    Private clienteID As Integer
    Public Property ID() As Integer
        Get
            Return clienteID
        End Get
        Set(value As Integer)
            clienteID = value
        End Set
    End Property

    Private clienteNombre As String
    Public Property Nombre() As String
        Get
            Return clienteNombre
        End Get
        Set(value As String)
            clienteNombre = value
        End Set
    End Property
    Private fechaRegistro As Date
    Public Property Fecha() As Date
        Get
            Return fechaRegistro
        End Get
        Set(value As Date)
            fechaRegistro = value
        End Set
    End Property
    Private clienteRamo As String
    Public Property Ramo() As String
        Get
            Return clienteRamo
        End Get
        Set(value As String)
            clienteRamo = value
        End Set
    End Property
    Private clienteLogin As String
    Public Property Login() As String
        Get
            Return clienteLogin
        End Get
        Set(value As String)
            clienteLogin = value
        End Set
    End Property
    Private clientePassword As String
    Public Property Password() As String
        Get
            Return clientePassword
        End Get
        Set(value As String)
            clientePassword = value
        End Set
    End Property

    'definicion y propiedad de la clave foranea
    Private listaDirecciones As List(Of Direccion)
    Public Property Direcciones() As List(Of Direccion)
        Get
            Return listaDirecciones
        End Get
        Set(value As List(Of Direccion))
            listaDirecciones = value
        End Set
    End Property
End Class
