﻿Imports Clientes.DAO
Imports ClienteSolucion

Public Class Direcciones
    Public Function ObtenerDireccions() As List(Of Direccion)
        Dim x As New DireccionesDAO
        Return x.ObtenerDirecciones()
    End Function

    Public Function btenerPorID(ByVal id As Integer) As List(Of Direccion)
        Dim x As New DireccionesDAO
        Return x.btenerPorID(id)
    End Function

    '---actualizar
    Public Sub UpdateDireccion(ByRef listaDireccions As LinkedList(Of Direccion))
        Dim x As New DireccionesDAO
        x.UpdateDireccion(listaDireccions)
    End Sub

    '---Elimianr
    Public Sub eliminarDireccions(ByVal listaDireccions As LinkedList(Of Direccion))
        Dim x As New DireccionesDAO
        x.eliminarDirecciones(listaDireccions)
    End Sub

    Public Sub eliminarDireccionsId(ByVal id As Integer)
        Dim x As New DireccionesDAO
        x.eliminarDireccionesId(id)
    End Sub

    '--insertar
    Public Sub InsertarDireccions(ByRef listaDireccions As LinkedList(Of Direccion))
        Dim x As New DireccionesDAO
        x.InsertarDirecciones(listaDireccions)
    End Sub

    Public Sub InsertarDireccions(ByRef DireccionObj As Direccion)
        Dim x As New DireccionesDAO
        x.InsertarDirecciones(DireccionObj)
    End Sub

End Class
