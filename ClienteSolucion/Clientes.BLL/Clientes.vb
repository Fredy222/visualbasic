﻿Imports Clientes.DAO
Imports ClienteSolucion

Public Class Clientes
    Public Function ObtenerClientes() As List(Of Cliente)
        Dim x As New ClientesDAO
        Return x.ObtenerClientes()
    End Function

    Public Function btenerPorID(ByVal id As Integer) As List(Of Cliente)
        Dim x As New ClientesDAO
        Return x.btenerPorID(id)
    End Function

    '---actualizar
    Public Sub UpdateCliente(ByRef listaClientes As LinkedList(Of Cliente))
        Dim x As New ClientesDAO
        x.UpdateCliente(listaClientes)
    End Sub

    '---Elimianr
    Public Sub eliminarClientes(ByVal listaClientes As LinkedList(Of Cliente))
        Dim x As New ClientesDAO
        x.eliminarClientes(listaClientes)
    End Sub

    Public Sub eliminarClientesId(ByVal id As Integer)
        Dim x As New ClientesDAO
        x.eliminarClientesId(id)
    End Sub

    '--insertar
    Public Sub InsertarClientes(ByRef listaClientes As LinkedList(Of Cliente))
        Dim x As New ClientesDAO
        x.InsertarClientes(listaClientes)
    End Sub

    Public Sub InsertarClientes(ByRef clienteObj As Cliente)
        Dim x As New ClientesDAO
        x.InsertarClientes(clienteObj)
    End Sub

End Class
