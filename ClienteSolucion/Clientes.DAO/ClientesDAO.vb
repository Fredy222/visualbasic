﻿Imports System.Data.SqlClient
Imports ClienteSolucion
Public Class ClientesDAO
    Inherits ClaseBaseDAO

    Public Function ObtenerClientes() As List(Of Cliente)
        Dim oCommand As New SqlCommand
        Try
            Dim listadoClientes As List(Of Cliente)
            oCommand.Connection = MyBase.obtenerConexion()
            oCommand.CommandText = "SELECT CLIENTE_ID,NOMBRE,FECHA_REGISTRO,RAMO,LOGIN,PASSWORD FROM TBL_CLIENTES"
            oCommand.Connection.Open()

            Dim oDataRead As SqlDataReader = oCommand.ExecuteReader()
            listadoClientes = RecuperarListado(oDataRead)
            oDataRead.Close()
            oCommand.Connection.Close()
            Return listadoClientes
        Catch ex As Exception
            Throw New System.ApplicationException("Nose pudo obtener el listado")
        End Try
    End Function

    Private Function RecuperarListado(ByVal dataRead As SqlDataReader) As List(Of Cliente)
        Try
            Dim listadoClientes As New List(Of Cliente)
            While (dataRead.Read)
                Dim x As New Cliente
                x.ID = dataRead.GetInt32(0)
                x.Nombre = dataRead.GetString(1)
                x.Fecha = dataRead.GetDateTime(2)
                x.Ramo = dataRead.GetString(3)
                x.Login = dataRead.GetString(4)
                x.Login = dataRead.GetString(5)
                listadoClientes.Add(x)

            End While
            Return listadoClientes
        Catch ex As Exception
            Throw New System.Exception("Ocurrio un problema")
        End Try
    End Function

    Public Function btenerPorID(ByVal id As Integer) As List(Of Cliente)
        Dim oCommand As New SqlCommand
        Try
            Dim listadoClientes As List(Of Cliente)
            oCommand.Connection = MyBase.obtenerConexion()
            oCommand.CommandText = "SELECT CLIENTE_ID,NOMBRE,FECHA_REGISTRO,RAMO,LOGIN,PASSWORD FROM TBL_CLIENTES WHERE CLIENTE_ID=@clienteId"
            oCommand.Parameters.Add("@clienteId", SqlDbType.Int).Value = id
            oCommand.Connection.Open()

            Dim oDataRead As SqlDataReader = oCommand.ExecuteReader()
            listadoClientes = RecuperarListado(oDataRead)
            oDataRead.Close()
            oCommand.Connection.Close()
            Return listadoClientes
        Catch ex As Exception
            Throw New System.ApplicationException
        Finally
            If oCommand.Connection.State = ConnectionState.Open Then
                oCommand.Connection.Close()
            End If
        End Try
    End Function

    '---actualizar
    Public Sub UpdateCliente(ByRef listaClientes As LinkedList(Of Cliente))
        Dim oCommand As New SqlCommand
        Try
            oCommand.Connection = MyBase.obtenerConexion()
            oCommand.CommandText = "UPDATE TBL_CLIENTES SET NOMBRE=@nombre,FECHA_REGISTRO=@fecha,RAMO=@ramo,LOGIN=@login,PASSWORD=@password WHERE CLIENTE_ID=@clienteId"
            oCommand.Parameters.Add("@clienteId", SqlDbType.Int)
            oCommand.Parameters.Add("@nombre", SqlDbType.VarChar)
            oCommand.Parameters.Add("@fecha", SqlDbType.Date)
            oCommand.Parameters.Add("@ramo", SqlDbType.VarChar)
            oCommand.Parameters.Add("@login", SqlDbType.VarChar)
            oCommand.Parameters.Add("@password", SqlDbType.VarChar)
            oCommand.Connection.Open()
            For Each x As Cliente In listaClientes
                oCommand.Parameters("@clienteId").Value = x.ID
                oCommand.Parameters("@nombre").Value = x.Nombre
                oCommand.Parameters("@fecha").Value = x.Fecha
                oCommand.Parameters("@ramo").Value = x.Ramo
                oCommand.Parameters("@login").Value = x.Login
                oCommand.Parameters("@password").Value = x.Password

                oCommand.ExecuteNonQuery()
            Next
            oCommand.Connection.Close()
        Catch ex As Exception
            Throw New System.Exception("No se pudo actualizar")
        Finally
            If oCommand.Connection.State = ConnectionState.Open Then
                oCommand.Connection.Close()
            End If
        End Try
    End Sub

    '---Elimianr
    Public Sub eliminarClientes(ByVal listaClientes As LinkedList(Of Cliente))
        Dim oCommand As New SqlCommand
        Try
            oCommand.Connection = MyBase.obtenerConexion()
            oCommand.CommandText = "DELETE FROM TBL_CLIENTES WHERE CLIENTE_ID=@clienteId"
            oCommand.Parameters.Add("@clienteId", SqlDbType.Int)

            oCommand.Connection.Open()
            For Each x As Cliente In listaClientes
                oCommand.Parameters("@clienteId").Value = x.ID
                oCommand.ExecuteNonQuery()
            Next
            oCommand.Connection.Close()
        Catch ex As Exception
            Throw New System.Exception("No se pudo Eliminar el registro")
        Finally
            If oCommand.Connection.State = ConnectionState.Open Then
                oCommand.Connection.Close()
            End If
        End Try
    End Sub

    Public Sub eliminarClientesId(ByVal id As Integer)
        Dim oCommand As New SqlCommand
        Try
            oCommand.Connection = MyBase.obtenerConexion()
            oCommand.CommandText = "DELETE FROM TBL_CLIENTES WHERE CLIENTE_ID=@clienteId"
            oCommand.Parameters.Add("@clienteId", SqlDbType.Int).Value = id

            oCommand.Connection.Open()
            oCommand.ExecuteNonQuery()

            oCommand.Connection.Close()
        Catch ex As Exception
            Throw New System.Exception("No se pudo Eliminar el registro")
        Finally
            If oCommand.Connection.State = ConnectionState.Open Then
                oCommand.Connection.Close()
            End If
        End Try
    End Sub

    '--insertar
    Public Sub InsertarClientes(ByRef listaClientes As LinkedList(Of Cliente))
        Dim oCommand As New SqlCommand
        Try
            oCommand.Connection = MyBase.obtenerConexion()
            oCommand.CommandText = "INSERT INTO  TBL_CLIENTES VALUES(@clienteId,@nombre,@fecha,@ramo,@login,@password )"
            oCommand.Parameters.Add("@clienteId", SqlDbType.Int)
            oCommand.Parameters.Add("@nombre", SqlDbType.VarChar)
            oCommand.Parameters.Add("@fecha", SqlDbType.Date)
            oCommand.Parameters.Add("@ramo", SqlDbType.VarChar)
            oCommand.Parameters.Add("@login", SqlDbType.VarChar)
            oCommand.Parameters.Add("@password", SqlDbType.VarChar)
            oCommand.Connection.Open()
            For Each x As Cliente In listaClientes
                oCommand.Parameters("@clienteId").Value = CapturarIdFibnal()
                oCommand.Parameters("@nombre").Value = x.Nombre
                oCommand.Parameters("@fecha").Value = x.Fecha
                oCommand.Parameters("@ramo").Value = x.Ramo
                oCommand.Parameters("@login").Value = x.Login
                oCommand.Parameters("@password").Value = x.Password

                oCommand.ExecuteNonQuery()
            Next
            oCommand.Connection.Close()
        Catch ex As Exception
            Throw New System.Exception("No se pudo insertar el registro")
        Finally
            If oCommand.Connection.State = ConnectionState.Open Then
                oCommand.Connection.Close()
            End If
        End Try
    End Sub

    Public Sub InsertarClientes(ByRef clienteObj As Cliente)
        Dim oCommand As New SqlCommand
        Try
            oCommand.Connection = MyBase.obtenerConexion()
            oCommand.CommandText = "INSERT INTO  TBL_CLIENTES VALUES(@clienteId,@nombre,@fecha,@ramo,@login,@password )"
            oCommand.Parameters.Add("@clienteId", SqlDbType.Int)
            oCommand.Parameters.Add("@nombre", SqlDbType.VarChar)
            oCommand.Parameters.Add("@fecha", SqlDbType.Date)
            oCommand.Parameters.Add("@ramo", SqlDbType.VarChar)
            oCommand.Parameters.Add("@login", SqlDbType.VarChar)
            oCommand.Parameters.Add("@password", SqlDbType.VarChar)
            oCommand.Connection.Open()

            oCommand.Parameters("@clienteId").Value = CapturarIdFibnal()
            oCommand.Parameters("@nombre").Value = clienteObj.Nombre
            oCommand.Parameters("@fecha").Value = clienteObj.Fecha
            oCommand.Parameters("@ramo").Value = clienteObj.Ramo
            oCommand.Parameters("@login").Value = clienteObj.Login
            oCommand.Parameters("@password").Value = clienteObj.Password

            oCommand.ExecuteNonQuery()

            oCommand.Connection.Close()
        Catch ex As Exception
            Throw New System.Exception("No se pudo insertar el registro ", ex)
        Finally
            If oCommand.Connection.State = ConnectionState.Open Then
                oCommand.Connection.Close()
            End If
        End Try
    End Sub

    '-- Para recuperar id 
    Private Function CapturarIdFibnal() As Integer
        Dim oCommand As New SqlCommand
        Try
            oCommand.Connection = MyBase.obtenerConexion()
            oCommand.CommandText = "SELECT MAX(CLIENTE_ID) FROM TBL_CLIENTES"
            oCommand.Connection.Open()
            Dim ultiom As Object = oCommand.ExecuteScalar()
            If ultiom Is Nothing Then
                ultiom = 1
            Else
                ultiom += 1
            End If
            Return Convert.ToInt32(ultiom)
            oCommand.Connection.Close()
        Catch ex As Exception
            Throw New System.Exception("No se pudo RECUPERAR EL REGISTRO")
        Finally
            If oCommand.Connection.State = ConnectionState.Open Then
                oCommand.Connection.Close()
            End If
        End Try
    End Function
End Class
