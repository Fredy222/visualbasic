﻿Imports System.Data.SqlClient
Imports ClienteSolucion
Public Class DireccionesDAO
    Inherits ClaseBaseDAO

    Public Function ObtenerDirecciones() As List(Of Direccion)
        Dim oCommand As New SqlCommand
        Try
            Dim listadoDirecciones As List(Of Direccion)
            oCommand.Connection = MyBase.obtenerConexion()
            oCommand.CommandText = "SELECT DIRECCION_ID,DIRECCION,CIUDAD,ESTADO,TELEFONO,CELULAR,RIF,PUNTO_REFERENCIA FROM TBL_DIRECCIONES"
            oCommand.Connection.Open()

            Dim oDataRead As SqlDataReader = oCommand.ExecuteReader()
            listadoDirecciones = RecuperarListado(oDataRead)
            oDataRead.Close()
            oCommand.Connection.Close()
            Return listadoDirecciones
        Catch ex As Exception
            Throw New System.ApplicationException("Nose pudo obtener el listado")
        End Try
    End Function

    Public Function RecuperarListado(ByVal dataRead As SqlDataReader) As List(Of Direccion)
        Try
            Dim listadoDirecciones As New List(Of Direccion)
            While (dataRead.Read)
                Dim x As New Direccion
                x.ID = dataRead.GetInt32(0)
                x.Direccion = dataRead.GetString(1)
                x.Ciudad = dataRead.GetString(2)
                x.Estado = dataRead.GetString(3)
                x.Telefono = dataRead.GetString(4)
                x.Celular = dataRead.GetString(5)
                x.Rif = dataRead.GetString(6)
                x.PuntoReferencia = dataRead.GetString(7)

                listadoDirecciones.Add(x)

            End While
            Return listadoDirecciones
        Catch ex As Exception
            Throw New System.Exception("Ocurrio un problema")
        End Try
    End Function

    Public Function btenerPorID(ByVal id As Integer) As List(Of Direccion)
        Dim oCommand As New SqlCommand
        Try
            Dim listadoDirecciones As List(Of Direccion)
            oCommand.Connection = MyBase.obtenerConexion()
            oCommand.CommandText = "SELECT CLEINTE_ID,NOMBRE,FECHA_REGISTRO,RAMO,LOGIN,PASSWORD FROM TBL_DIRECCIONES WHERE Direccion_ID=@DireccionId"
            oCommand.Parameters.Add("@DireccionId", SqlDbType.Int).Value = id
            oCommand.Connection.Open()

            Dim oDataRead As SqlDataReader = oCommand.ExecuteReader()
            listadoDirecciones = RecuperarListado(oDataRead)
            oDataRead.Close()
            oCommand.Connection.Close()
            Return listadoDirecciones
        Catch ex As Exception
            Throw New System.ApplicationException
        Finally
            If oCommand.Connection.State = ConnectionState.Open Then
                oCommand.Connection.Close()
            End If
        End Try
    End Function

    '---actualizar
    Public Sub UpdateDireccion(ByRef listaDirecciones As LinkedList(Of Direccion))
        Dim oCommand As New SqlCommand
        Try
            oCommand.Connection = MyBase.obtenerConexion()
            oCommand.CommandText = "UPDATE TBL_DIRECCIONES SET CLIENTE_ID=@cliente,DIRECCION=@direccion,CIUDAD=@ciudad,ESTADO=@estado,TELEFONO=@telefono,CELULAR=@celular,
            RIF=@rif,PUNTO_REFERENCIA=@referencia WHERE Direccion_ID=@DireccionId"
            oCommand.Parameters.Add("@DireccionId", SqlDbType.Int)
            '---
            oCommand.Parameters.Add("@cliente", SqlDbType.Int)
            '--
            oCommand.Parameters.Add("@direccion", SqlDbType.VarChar)
            oCommand.Parameters.Add("@ciudad", SqlDbType.VarChar)
            oCommand.Parameters.Add("@estado", SqlDbType.VarChar)
            oCommand.Parameters.Add("@telefono", SqlDbType.VarChar)
            oCommand.Parameters.Add("@celular", SqlDbType.VarChar)
            oCommand.Parameters.Add("@rif", SqlDbType.VarChar)
            oCommand.Parameters.Add("@referencia", SqlDbType.VarChar)
            oCommand.Connection.Open()
            For Each x As Direccion In listaDirecciones
                oCommand.Parameters("@DireccionId").Value = x.ID
                oCommand.Parameters("@direccion").Value = x.Direccion
                oCommand.Parameters("@ciudad").Value = x.Celular
                oCommand.Parameters("@estado").Value = x.Estado
                oCommand.Parameters("@telefono").Value = x.Telefono
                oCommand.Parameters("@celular").Value = x.Celular
                oCommand.Parameters("@rif").Value = x.Rif
                oCommand.Parameters("@referencia").Value = x.PuntoReferencia
                oCommand.ExecuteNonQuery()
            Next
            oCommand.Connection.Close()
        Catch ex As Exception
            Throw New System.Exception("No se pudo actualizar")
        Finally
            If oCommand.Connection.State = ConnectionState.Open Then
                oCommand.Connection.Close()
            End If
        End Try
    End Sub

    '---Elimianr
    Public Sub eliminarDirecciones(ByVal listaDirecciones As LinkedList(Of Direccion))
        Dim oCommand As New SqlCommand
        Try
            oCommand.Connection = MyBase.obtenerConexion()
            oCommand.CommandText = "DELETE FROM TBL_DIRECCIONES WHERE Direccion_ID=@DireccionId"
            oCommand.Parameters.Add("@DireccionId", SqlDbType.Int)

            oCommand.Connection.Open()
            For Each x As Direccion In listaDirecciones
                oCommand.Parameters("@DireccionId").Value = x.ID
                oCommand.ExecuteNonQuery()
            Next
            oCommand.Connection.Close()
        Catch ex As Exception
            Throw New System.Exception("No se pudo Eliminar el registro")
        Finally
            If oCommand.Connection.State = ConnectionState.Open Then
                oCommand.Connection.Close()
            End If
        End Try
    End Sub

    Public Sub eliminarDireccionesId(ByVal id As Integer)
        Dim oCommand As New SqlCommand
        Try
            oCommand.Connection = MyBase.obtenerConexion()
            oCommand.CommandText = "DELETE FROM TBL_DIRECCIONES WHERE Direccion_ID=@DireccionId"
            oCommand.Parameters.Add("@DireccionId", SqlDbType.Int).Value = id

            oCommand.Connection.Open()
            oCommand.ExecuteNonQuery()

            oCommand.Connection.Close()
        Catch ex As Exception
            Throw New System.Exception("No se pudo Eliminar el registro")
        Finally
            If oCommand.Connection.State = ConnectionState.Open Then
                oCommand.Connection.Close()
            End If
        End Try
    End Sub

    '--insertar
    Public Sub InsertarDirecciones(ByRef listaDirecciones As LinkedList(Of Direccion))
        Dim oCommand As New SqlCommand
        Try
            oCommand.Connection = MyBase.obtenerConexion()
            oCommand.CommandText = "INSERT INTO  TBL_DIRECCIONES VALUES(@DireccionId,@cliente,@ciudad,@estado,@telefono,@celular,@rif,@referencia)"
            oCommand.Parameters.Add("@DireccionId", SqlDbType.Int)
            '---
            oCommand.Parameters.Add("@cliente", SqlDbType.Int)
            '--
            oCommand.Parameters.Add("@direccion", SqlDbType.VarChar)
            oCommand.Parameters.Add("@ciudad", SqlDbType.VarChar)
            oCommand.Parameters.Add("@estado", SqlDbType.VarChar)
            oCommand.Parameters.Add("@telefono", SqlDbType.VarChar)
            oCommand.Parameters.Add("@celular", SqlDbType.VarChar)
            oCommand.Parameters.Add("@rif", SqlDbType.VarChar)
            oCommand.Parameters.Add("@referencia", SqlDbType.VarChar)
            oCommand.Connection.Open()
            For Each x As Direccion In listaDirecciones
                oCommand.Parameters("@DireccionId").Value = CapturarIdFibnal()
                oCommand.Parameters("@direccion").Value = x.Direccion
                oCommand.Parameters("@ciudad").Value = x.Celular
                oCommand.Parameters("@estado").Value = x.Estado
                oCommand.Parameters("@telefono").Value = x.Telefono
                oCommand.Parameters("@celular").Value = x.Celular
                oCommand.Parameters("@rif").Value = x.Rif
                oCommand.Parameters("@referencia").Value = x.PuntoReferencia

                oCommand.ExecuteNonQuery()
            Next
            oCommand.Connection.Close()
        Catch ex As Exception
            Throw New System.Exception("No se pudo insertar el registro")
        Finally
            If oCommand.Connection.State = ConnectionState.Open Then
                oCommand.Connection.Close()
            End If
        End Try
    End Sub

    Public Sub InsertarDirecciones(ByRef direccionObj As Direccion)
        Dim oCommand As New SqlCommand
        Try
            oCommand.Connection = MyBase.obtenerConexion()
            oCommand.CommandText = "INSERT INTO  TBL_DIRECCIONES VALUES(@DireccionId,@cliente,@ciudad,@estado,@telefono,@celular,@rif,@referencia)"
            oCommand.Parameters.Add("@DireccionId", SqlDbType.Int)
            '---
            oCommand.Parameters.Add("@cliente", SqlDbType.Int)
            '--
            oCommand.Parameters.Add("@direccion", SqlDbType.VarChar)
            oCommand.Parameters.Add("@ciudad", SqlDbType.VarChar)
            oCommand.Parameters.Add("@estado", SqlDbType.VarChar)
            oCommand.Parameters.Add("@telefono", SqlDbType.VarChar)
            oCommand.Parameters.Add("@celular", SqlDbType.VarChar)
            oCommand.Parameters.Add("@rif", SqlDbType.VarChar)
            oCommand.Parameters.Add("@referencia", SqlDbType.VarChar)
            oCommand.Connection.Open()

            oCommand.Parameters("@DireccionId").Value = CapturarIdFibnal()
            oCommand.Parameters("@direccion").Value = direccionObj.Direccion
            oCommand.Parameters("@ciudad").Value = direccionObj.Celular
            oCommand.Parameters("@estado").Value = direccionObj.Estado
            oCommand.Parameters("@telefono").Value = direccionObj.Telefono
            oCommand.Parameters("@celular").Value = direccionObj.Celular
            oCommand.Parameters("@rif").Value = direccionObj.Rif
            oCommand.Parameters("@referencia").Value = direccionObj.PuntoReferencia
            oCommand.ExecuteNonQuery()

            oCommand.Connection.Close()
        Catch ex As Exception
            Throw New System.Exception("No se pudo insertar el registro ", ex)
        Finally
            If oCommand.Connection.State = ConnectionState.Open Then
                oCommand.Connection.Close()
            End If
        End Try
    End Sub

    '-- Para recuperar id 
    Private Function CapturarIdFibnal() As Integer
        Dim oCommand As New SqlCommand
        Try
            oCommand.Connection = MyBase.obtenerConexion()
            oCommand.CommandText = "SELECT MAX(Direccion_ID) FROM TBL_DIRECCIONES"
            oCommand.Connection.Open()
            Dim ultiom As Object = oCommand.ExecuteScalar()
            If ultiom Is Nothing Then
                ultiom = 1
            Else
                ultiom += 1
            End If
            Return Convert.ToInt32(ultiom)
            oCommand.Connection.Close()
        Catch ex As Exception
            Throw New System.Exception("No se pudo RECUPERAR EL REGISTRO")
        Finally
            If oCommand.Connection.State = ConnectionState.Open Then
                oCommand.Connection.Close()
            End If
        End Try
    End Function
End Class
