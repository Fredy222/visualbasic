﻿Imports System.Data.SqlClient
Public Class FormularioPrincipal
    Private Sub btnOpcion1_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnOpcion1.ItemClick
        Dim forms As New Opcion1
        forms.MdiParent = Me
        forms.Show()
    End Sub

    Private Sub btnOpcion2_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnOpcion2.ItemClick
        Dim forms As New Opcion2
        forms.MdiParent = Me
        forms.Show()
    End Sub

    Private Sub btnOpcion3_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles btnOpcion3.ItemClick
        Dim forms As New Opcion3
        forms.MdiParent = Me
        forms.Show()
    End Sub

    Private Sub nOpcopn1_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nOpcopn1.LinkClicked
        Dim forms As New Opcion1
        forms.MdiParent = Me
        forms.Show()
    End Sub

    Private Sub nOpcopn2_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nOpcopn2.LinkClicked
        Dim forms As New Opcion2
        forms.MdiParent = Me
        forms.Show()
    End Sub

    Private Sub nOpcopn3_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nOpcopn3.LinkClicked
        Dim forms As New Opcion3
        forms.MdiParent = Me
        forms.Show()
    End Sub

    Private Sub nReporte1_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nReporte1.LinkClicked
        'Dim fmReporte As New rReporte
        'Dim Reporte1 As New XtraReport1
        'Reporte1.DataSource = ConsultaProduccion()
        'Reporte1.CreateDocument()

        'fmReporte.printControl.printingSystem = Reporte1.PrintingSystem
        Dim objReporte As New XtraReport1
        Dim pTtool As New DevExpress.XtraReports.UI.ReportPrintTool(objReporte)
        pTtool.ShowPreview()

    End Sub

    Private Function ConsultaProduccion() As DataSet
        Dim ds As New DataSet
        Dim cn As New SqlConnection()
        cn.ConnectionString = "Data Source=localhost;Initial Catalog=ECOFFEETECH01;User ID=sa;Password=Egsa2020"
        Dim da As New SqlDataAdapter
        Dim cm As New SqlCommand
        cm.Connection = cn
        cm.CommandType = CommandType.Text
        cm.CommandText = "select * from V_CONSULTA8"
        ' cm.Parameters.Add("")
        da.SelectCommand = cm
        da.Fill(ds, "V_CONSULTA8")
        Return ds
    End Function

    Private Sub nbPlanillas_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nbPlanillas.LinkClicked
        Dim forma As New frmTreeList
        forma.MdiParent = Me
        forma.Show()
    End Sub
End Class