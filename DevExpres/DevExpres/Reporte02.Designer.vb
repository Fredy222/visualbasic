﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Reporte02
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Ds_Produccion = New DevExpres.ds_Produccion()
        Me.GENEMPLEADOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GENEMPLEADOBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCORR_EMPRESA = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCORR_EMPLEADO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colNOMBRE_EMPLEADO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDOCUMENTO_IDENTIDAD = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCORR_TIPO_EMPLEADO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFECHA_INGRESO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSEXO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDIRECCION_PARTICULAR = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSALARIO_MENSUAL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCORR_PUESTO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colESTADO_ACTIVO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.riuDocumento = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        CType(Me.Ds_Produccion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GENEMPLEADOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GENEMPLEADOBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.riuDocumento, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Ds_Produccion
        '
        Me.Ds_Produccion.DataSetName = "ds_Produccion"
        Me.Ds_Produccion.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GENEMPLEADOBindingSource
        '
        Me.GENEMPLEADOBindingSource.DataMember = "GEN_EMPLEADO"
        Me.GENEMPLEADOBindingSource.DataSource = Me.Ds_Produccion
        '
        'GENEMPLEADOBindingSource1
        '
        Me.GENEMPLEADOBindingSource1.DataMember = "GEN_EMPLEADO"
        Me.GENEMPLEADOBindingSource1.DataSource = Me.Ds_Produccion
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.GENEMPLEADOBindingSource
        Me.GridControl1.Location = New System.Drawing.Point(-23, 33)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.riuDocumento})
        Me.GridControl1.Size = New System.Drawing.Size(578, 200)
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCORR_EMPRESA, Me.colCORR_EMPLEADO, Me.colNOMBRE_EMPLEADO, Me.colDOCUMENTO_IDENTIDAD, Me.colCORR_TIPO_EMPLEADO, Me.colFECHA_INGRESO, Me.colSEXO, Me.colDIRECCION_PARTICULAR, Me.colSALARIO_MENSUAL, Me.colCORR_PUESTO, Me.colESTADO_ACTIVO})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.NewItemRowText = "Click aqui para agregar nuevo registro"
        Me.GridView1.OptionsView.ColumnAutoWidth = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'colCORR_EMPRESA
        '
        Me.colCORR_EMPRESA.FieldName = "CORR_EMPRESA"
        Me.colCORR_EMPRESA.Name = "colCORR_EMPRESA"
        Me.colCORR_EMPRESA.OptionsColumn.ReadOnly = True
        Me.colCORR_EMPRESA.Width = 50
        '
        'colCORR_EMPLEADO
        '
        Me.colCORR_EMPLEADO.Caption = "Codigo"
        Me.colCORR_EMPLEADO.FieldName = "CORR_EMPLEADO"
        Me.colCORR_EMPLEADO.Name = "colCORR_EMPLEADO"
        Me.colCORR_EMPLEADO.OptionsColumn.ReadOnly = True
        Me.colCORR_EMPLEADO.Visible = True
        Me.colCORR_EMPLEADO.VisibleIndex = 0
        Me.colCORR_EMPLEADO.Width = 49
        '
        'colNOMBRE_EMPLEADO
        '
        Me.colNOMBRE_EMPLEADO.Caption = "Nombre"
        Me.colNOMBRE_EMPLEADO.FieldName = "NOMBRE_EMPLEADO"
        Me.colNOMBRE_EMPLEADO.Name = "colNOMBRE_EMPLEADO"
        Me.colNOMBRE_EMPLEADO.OptionsColumn.ReadOnly = True
        Me.colNOMBRE_EMPLEADO.Visible = True
        Me.colNOMBRE_EMPLEADO.VisibleIndex = 1
        Me.colNOMBRE_EMPLEADO.Width = 99
        '
        'colDOCUMENTO_IDENTIDAD
        '
        Me.colDOCUMENTO_IDENTIDAD.Caption = "Documento"
        Me.colDOCUMENTO_IDENTIDAD.ColumnEdit = Me.riuDocumento
        Me.colDOCUMENTO_IDENTIDAD.FieldName = "DOCUMENTO_IDENTIDAD"
        Me.colDOCUMENTO_IDENTIDAD.Name = "colDOCUMENTO_IDENTIDAD"
        Me.colDOCUMENTO_IDENTIDAD.OptionsColumn.ReadOnly = True
        Me.colDOCUMENTO_IDENTIDAD.Visible = True
        Me.colDOCUMENTO_IDENTIDAD.VisibleIndex = 2
        '
        'colCORR_TIPO_EMPLEADO
        '
        Me.colCORR_TIPO_EMPLEADO.FieldName = "CORR_TIPO_EMPLEADO"
        Me.colCORR_TIPO_EMPLEADO.Name = "colCORR_TIPO_EMPLEADO"
        Me.colCORR_TIPO_EMPLEADO.OptionsColumn.ReadOnly = True
        Me.colCORR_TIPO_EMPLEADO.Width = 63
        '
        'colFECHA_INGRESO
        '
        Me.colFECHA_INGRESO.Caption = "Fecha Ingreso"
        Me.colFECHA_INGRESO.FieldName = "FECHA_INGRESO"
        Me.colFECHA_INGRESO.Name = "colFECHA_INGRESO"
        Me.colFECHA_INGRESO.OptionsColumn.ReadOnly = True
        Me.colFECHA_INGRESO.Visible = True
        Me.colFECHA_INGRESO.VisibleIndex = 3
        Me.colFECHA_INGRESO.Width = 80
        '
        'colSEXO
        '
        Me.colSEXO.Caption = "Sexo"
        Me.colSEXO.FieldName = "SEXO"
        Me.colSEXO.Name = "colSEXO"
        Me.colSEXO.OptionsColumn.ReadOnly = True
        Me.colSEXO.Visible = True
        Me.colSEXO.VisibleIndex = 4
        Me.colSEXO.Width = 44
        '
        'colDIRECCION_PARTICULAR
        '
        Me.colDIRECCION_PARTICULAR.Caption = "Direccion"
        Me.colDIRECCION_PARTICULAR.FieldName = "DIRECCION_PARTICULAR"
        Me.colDIRECCION_PARTICULAR.Name = "colDIRECCION_PARTICULAR"
        Me.colDIRECCION_PARTICULAR.OptionsColumn.ReadOnly = True
        Me.colDIRECCION_PARTICULAR.Visible = True
        Me.colDIRECCION_PARTICULAR.VisibleIndex = 5
        '
        'colSALARIO_MENSUAL
        '
        Me.colSALARIO_MENSUAL.Caption = "Salario Mensual"
        Me.colSALARIO_MENSUAL.FieldName = "SALARIO_MENSUAL"
        Me.colSALARIO_MENSUAL.Name = "colSALARIO_MENSUAL"
        Me.colSALARIO_MENSUAL.OptionsColumn.ReadOnly = True
        Me.colSALARIO_MENSUAL.Visible = True
        Me.colSALARIO_MENSUAL.VisibleIndex = 6
        Me.colSALARIO_MENSUAL.Width = 78
        '
        'colCORR_PUESTO
        '
        Me.colCORR_PUESTO.FieldName = "CORR_PUESTO"
        Me.colCORR_PUESTO.Name = "colCORR_PUESTO"
        Me.colCORR_PUESTO.OptionsColumn.ReadOnly = True
        Me.colCORR_PUESTO.Width = 56
        '
        'colESTADO_ACTIVO
        '
        Me.colESTADO_ACTIVO.FieldName = "ESTADO_ACTIVO"
        Me.colESTADO_ACTIVO.Name = "colESTADO_ACTIVO"
        Me.colESTADO_ACTIVO.OptionsColumn.ReadOnly = True
        Me.colESTADO_ACTIVO.Width = 55
        '
        'riuDocumento
        '
        Me.riuDocumento.AutoHeight = False
        Me.riuDocumento.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.riuDocumento.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("DOCUMENTO", "DOCUMENTO"), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("Descripcion", "Descripcion")})
        Me.riuDocumento.Name = "riuDocumento"
        Me.riuDocumento.NullText = ""
        '
        'Reporte02
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(619, 307)
        Me.Controls.Add(Me.GridControl1)
        Me.Name = "Reporte02"
        Me.Text = "Reporte02"
        CType(Me.Ds_Produccion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GENEMPLEADOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GENEMPLEADOBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.riuDocumento, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Ds_Produccion As ds_Produccion
    Friend WithEvents GENEMPLEADOBindingSource As BindingSource
    Friend WithEvents GENEMPLEADOBindingSource1 As BindingSource
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colCORR_EMPRESA As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCORR_EMPLEADO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNOMBRE_EMPLEADO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDOCUMENTO_IDENTIDAD As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents riuDocumento As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents colCORR_TIPO_EMPLEADO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFECHA_INGRESO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSEXO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDIRECCION_PARTICULAR As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSALARIO_MENSUAL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCORR_PUESTO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colESTADO_ACTIVO As DevExpress.XtraGrid.Columns.GridColumn
End Class
