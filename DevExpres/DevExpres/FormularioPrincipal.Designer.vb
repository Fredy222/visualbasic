﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormularioPrincipal
    Inherits DevExpress.XtraBars.Ribbon.RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormularioPrincipal))
        Me.RibbonControl = New DevExpress.XtraBars.Ribbon.RibbonControl()
        Me.btnOpcion1 = New DevExpress.XtraBars.BarButtonItem()
        Me.btnOpcion2 = New DevExpress.XtraBars.BarButtonItem()
        Me.btnOpcion3 = New DevExpress.XtraBars.BarButtonItem()
        Me.Lista = New DevExpress.XtraBars.BarMdiChildrenListItem()
        Me.rpPrincipal = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.rpOpciones = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonStatusBar = New DevExpress.XtraBars.Ribbon.RibbonStatusBar()
        Me.XtraTabbedMdiManager1 = New DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(Me.components)
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        Me.NavBarControl1 = New DevExpress.XtraNavBar.NavBarControl()
        Me.nGroup = New DevExpress.XtraNavBar.NavBarGroup()
        Me.nOpcopn1 = New DevExpress.XtraNavBar.NavBarItem()
        Me.nOpcopn2 = New DevExpress.XtraNavBar.NavBarItem()
        Me.nOpcopn3 = New DevExpress.XtraNavBar.NavBarItem()
        Me.nReportes = New DevExpress.XtraNavBar.NavBarGroup()
        Me.nReporte1 = New DevExpress.XtraNavBar.NavBarItem()
        Me.nReporte2 = New DevExpress.XtraNavBar.NavBarItem()
        Me.nGroup3 = New DevExpress.XtraNavBar.NavBarGroup()
        Me.nbPlanillas = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavBarItem2 = New DevExpress.XtraNavBar.NavBarItem()
        Me.NavBarItem3 = New DevExpress.XtraNavBar.NavBarItem()
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.XtraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RibbonControl
        '
        Me.RibbonControl.ApplicationIcon = CType(resources.GetObject("RibbonControl.ApplicationIcon"), System.Drawing.Bitmap)
        Me.RibbonControl.ExpandCollapseItem.Id = 0
        Me.RibbonControl.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.RibbonControl.ExpandCollapseItem, Me.btnOpcion1, Me.btnOpcion2, Me.btnOpcion3, Me.Lista})
        Me.RibbonControl.Location = New System.Drawing.Point(0, 0)
        Me.RibbonControl.MaxItemId = 5
        Me.RibbonControl.Name = "RibbonControl"
        Me.RibbonControl.Pages.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPage() {Me.rpPrincipal})
        Me.RibbonControl.Size = New System.Drawing.Size(502, 143)
        Me.RibbonControl.StatusBar = Me.RibbonStatusBar
        '
        'btnOpcion1
        '
        Me.btnOpcion1.Caption = "Opcion1"
        Me.btnOpcion1.Id = 1
        Me.btnOpcion1.ImageOptions.AllowStubGlyph = DevExpress.Utils.DefaultBoolean.[True]
        Me.btnOpcion1.ImageOptions.DisabledImage = CType(resources.GetObject("btnOpcion1.ImageOptions.DisabledImage"), System.Drawing.Image)
        Me.btnOpcion1.ImageOptions.LargeImage = CType(resources.GetObject("btnOpcion1.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.btnOpcion1.Name = "btnOpcion1"
        '
        'btnOpcion2
        '
        Me.btnOpcion2.Caption = "Opcion2"
        Me.btnOpcion2.Id = 2
        Me.btnOpcion2.ImageOptions.DisabledImage = CType(resources.GetObject("btnOpcion2.ImageOptions.DisabledImage"), System.Drawing.Image)
        Me.btnOpcion2.ImageOptions.LargeImage = CType(resources.GetObject("btnOpcion2.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.btnOpcion2.Name = "btnOpcion2"
        '
        'btnOpcion3
        '
        Me.btnOpcion3.Caption = "Opcion3"
        Me.btnOpcion3.Id = 3
        Me.btnOpcion3.ImageOptions.DisabledImage = CType(resources.GetObject("btnOpcion3.ImageOptions.DisabledImage"), System.Drawing.Image)
        Me.btnOpcion3.ImageOptions.LargeImage = CType(resources.GetObject("btnOpcion3.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.btnOpcion3.Name = "btnOpcion3"
        '
        'Lista
        '
        Me.Lista.Caption = "Lista"
        Me.Lista.Id = 4
        Me.Lista.Name = "Lista"
        '
        'rpPrincipal
        '
        Me.rpPrincipal.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.rpOpciones})
        Me.rpPrincipal.Name = "rpPrincipal"
        Me.rpPrincipal.Text = "Principal"
        '
        'rpOpciones
        '
        Me.rpOpciones.ItemLinks.Add(Me.btnOpcion1)
        Me.rpOpciones.ItemLinks.Add(Me.btnOpcion2)
        Me.rpOpciones.ItemLinks.Add(Me.btnOpcion3)
        Me.rpOpciones.ItemLinks.Add(Me.Lista)
        Me.rpOpciones.Name = "rpOpciones"
        Me.rpOpciones.Text = "Opciones"
        '
        'RibbonStatusBar
        '
        Me.RibbonStatusBar.Location = New System.Drawing.Point(0, 418)
        Me.RibbonStatusBar.Name = "RibbonStatusBar"
        Me.RibbonStatusBar.Ribbon = Me.RibbonControl
        Me.RibbonStatusBar.Size = New System.Drawing.Size(502, 31)
        '
        'XtraTabbedMdiManager1
        '
        Me.XtraTabbedMdiManager1.MdiParent = Me
        '
        'NavBarControl1
        '
        Me.NavBarControl1.ActiveGroup = Me.nGroup
        Me.NavBarControl1.Dock = System.Windows.Forms.DockStyle.Left
        Me.NavBarControl1.Groups.AddRange(New DevExpress.XtraNavBar.NavBarGroup() {Me.nGroup, Me.nReportes, Me.nGroup3})
        Me.NavBarControl1.Items.AddRange(New DevExpress.XtraNavBar.NavBarItem() {Me.nOpcopn1, Me.nOpcopn2, Me.nOpcopn3, Me.nReporte1, Me.nReporte2, Me.nbPlanillas, Me.NavBarItem2, Me.NavBarItem3})
        Me.NavBarControl1.Location = New System.Drawing.Point(0, 143)
        Me.NavBarControl1.Name = "NavBarControl1"
        Me.NavBarControl1.OptionsNavPane.ExpandedWidth = 140
        Me.NavBarControl1.Size = New System.Drawing.Size(140, 275)
        Me.NavBarControl1.TabIndex = 3
        Me.NavBarControl1.Text = "NavBarControl1"
        '
        'nGroup
        '
        Me.nGroup.Caption = "Grupo 1"
        Me.nGroup.Expanded = True
        Me.nGroup.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.nOpcopn1), New DevExpress.XtraNavBar.NavBarItemLink(Me.nOpcopn2), New DevExpress.XtraNavBar.NavBarItemLink(Me.nOpcopn3)})
        Me.nGroup.Name = "nGroup"
        '
        'nOpcopn1
        '
        Me.nOpcopn1.Caption = "Opcion1"
        Me.nOpcopn1.Name = "nOpcopn1"
        '
        'nOpcopn2
        '
        Me.nOpcopn2.Caption = "Opcion2"
        Me.nOpcopn2.Name = "nOpcopn2"
        '
        'nOpcopn3
        '
        Me.nOpcopn3.Caption = "Opcopn3"
        Me.nOpcopn3.Name = "nOpcopn3"
        '
        'nReportes
        '
        Me.nReportes.Caption = "Reportes"
        Me.nReportes.Expanded = True
        Me.nReportes.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.nReporte1), New DevExpress.XtraNavBar.NavBarItemLink(Me.nReporte2)})
        Me.nReportes.Name = "nReportes"
        '
        'nReporte1
        '
        Me.nReporte1.Caption = "Reporte1"
        Me.nReporte1.Name = "nReporte1"
        '
        'nReporte2
        '
        Me.nReporte2.Caption = "Reporte2"
        Me.nReporte2.Name = "nReporte2"
        '
        'nGroup3
        '
        Me.nGroup3.Caption = "Grupo3"
        Me.nGroup3.Expanded = True
        Me.nGroup3.ItemLinks.AddRange(New DevExpress.XtraNavBar.NavBarItemLink() {New DevExpress.XtraNavBar.NavBarItemLink(Me.nbPlanillas), New DevExpress.XtraNavBar.NavBarItemLink(Me.NavBarItem2), New DevExpress.XtraNavBar.NavBarItemLink(Me.NavBarItem3)})
        Me.nGroup3.Name = "nGroup3"
        '
        'nbPlanillas
        '
        Me.nbPlanillas.Caption = "Planillas"
        Me.nbPlanillas.Name = "nbPlanillas"
        '
        'NavBarItem2
        '
        Me.NavBarItem2.Caption = "NavBarItem2"
        Me.NavBarItem2.Name = "NavBarItem2"
        '
        'NavBarItem3
        '
        Me.NavBarItem3.Caption = "NavBarItem3"
        Me.NavBarItem3.Name = "NavBarItem3"
        '
        'FormularioPrincipal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(502, 449)
        Me.Controls.Add(Me.NavBarControl1)
        Me.Controls.Add(Me.RibbonStatusBar)
        Me.Controls.Add(Me.RibbonControl)
        Me.IsMdiContainer = True
        Me.Name = "FormularioPrincipal"
        Me.Ribbon = Me.RibbonControl
        Me.StatusBar = Me.RibbonStatusBar
        Me.Text = "FormularioPrincipal"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.XtraTabbedMdiManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NavBarControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents RibbonControl As DevExpress.XtraBars.Ribbon.RibbonControl
    Friend WithEvents rpPrincipal As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents rpOpciones As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonStatusBar As DevExpress.XtraBars.Ribbon.RibbonStatusBar
    Friend WithEvents btnOpcion1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnOpcion2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnOpcion3 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents Lista As DevExpress.XtraBars.BarMdiChildrenListItem
    Friend WithEvents XtraTabbedMdiManager1 As DevExpress.XtraTabbedMdi.XtraTabbedMdiManager
    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents NavBarControl1 As DevExpress.XtraNavBar.NavBarControl
    Friend WithEvents nGroup As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents nReportes As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents nGroup3 As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents nOpcopn1 As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nOpcopn2 As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nOpcopn3 As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nReporte1 As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nReporte2 As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents nbPlanillas As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavBarItem2 As DevExpress.XtraNavBar.NavBarItem
    Friend WithEvents NavBarItem3 As DevExpress.XtraNavBar.NavBarItem
End Class
