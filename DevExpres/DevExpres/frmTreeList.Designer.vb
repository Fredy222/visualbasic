﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTreeList
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.TreeList1 = New DevExpress.XtraTreeList.TreeList()
        Me.ANIO_PERIODO = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        Me.MES_PERIODO = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        Me.CORR_PLANILLA = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        Me.FECHA_PLANILLA = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        Me.CORR_TIPO_PLANILLA = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        Me.FECHA_INICIAL = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        Me.FECHA_FINAL = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        Me.DIAS_PAGADOS = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        Me.ESTADO_PLANILLA = New DevExpress.XtraTreeList.Columns.TreeListColumn()
        Me.PLADOCUMENTOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.Ds_Produccion = New DevExpres.ds_Produccion()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.TreeList1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PLADOCUMENTOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Ds_Produccion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.TreeList1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.GroupControl1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(845, 258)
        Me.SplitContainerControl1.SplitterPosition = 546
        Me.SplitContainerControl1.TabIndex = 0
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'TreeList1
        '
        Me.TreeList1.Columns.AddRange(New DevExpress.XtraTreeList.Columns.TreeListColumn() {Me.ANIO_PERIODO, Me.MES_PERIODO, Me.CORR_PLANILLA, Me.FECHA_PLANILLA, Me.CORR_TIPO_PLANILLA, Me.FECHA_INICIAL, Me.FECHA_FINAL, Me.DIAS_PAGADOS, Me.ESTADO_PLANILLA})
        Me.TreeList1.Cursor = System.Windows.Forms.Cursors.Default
        Me.TreeList1.DataSource = Me.PLADOCUMENTOBindingSource
        Me.TreeList1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TreeList1.KeyFieldName = "CORR_PLANILLA"
        Me.TreeList1.Location = New System.Drawing.Point(0, 0)
        Me.TreeList1.Name = "TreeList1"
        Me.TreeList1.OptionsBehavior.PopulateServiceColumns = True
        Me.TreeList1.ParentFieldName = "CORR_EMPRESA"
        Me.TreeList1.RootValue = Nothing
        Me.TreeList1.Size = New System.Drawing.Size(546, 258)
        Me.TreeList1.TabIndex = 0
        '
        'ANIO_PERIODO
        '
        Me.ANIO_PERIODO.Caption = "Año"
        Me.ANIO_PERIODO.FieldName = "ANIO_PERIODO"
        Me.ANIO_PERIODO.Name = "ANIO_PERIODO"
        Me.ANIO_PERIODO.Visible = True
        Me.ANIO_PERIODO.VisibleIndex = 0
        Me.ANIO_PERIODO.Width = 57
        '
        'MES_PERIODO
        '
        Me.MES_PERIODO.Caption = "Mes"
        Me.MES_PERIODO.FieldName = "MES_PERIODO"
        Me.MES_PERIODO.Name = "MES_PERIODO"
        Me.MES_PERIODO.Visible = True
        Me.MES_PERIODO.VisibleIndex = 1
        Me.MES_PERIODO.Width = 47
        '
        'CORR_PLANILLA
        '
        Me.CORR_PLANILLA.Caption = "Planilla"
        Me.CORR_PLANILLA.FieldName = "CORR_PLANILLA"
        Me.CORR_PLANILLA.Name = "CORR_PLANILLA"
        Me.CORR_PLANILLA.Visible = True
        Me.CORR_PLANILLA.VisibleIndex = 2
        Me.CORR_PLANILLA.Width = 84
        '
        'FECHA_PLANILLA
        '
        Me.FECHA_PLANILLA.Caption = "Fecha"
        Me.FECHA_PLANILLA.FieldName = "FECHA_PLANILLA"
        Me.FECHA_PLANILLA.Name = "FECHA_PLANILLA"
        Me.FECHA_PLANILLA.Visible = True
        Me.FECHA_PLANILLA.VisibleIndex = 3
        Me.FECHA_PLANILLA.Width = 84
        '
        'CORR_TIPO_PLANILLA
        '
        Me.CORR_TIPO_PLANILLA.Caption = "Tipo"
        Me.CORR_TIPO_PLANILLA.FieldName = "CORR_TIPO_PLANILLA"
        Me.CORR_TIPO_PLANILLA.Name = "CORR_TIPO_PLANILLA"
        Me.CORR_TIPO_PLANILLA.Visible = True
        Me.CORR_TIPO_PLANILLA.VisibleIndex = 4
        Me.CORR_TIPO_PLANILLA.Width = 67
        '
        'FECHA_INICIAL
        '
        Me.FECHA_INICIAL.Caption = "Fecha Inicial"
        Me.FECHA_INICIAL.FieldName = "FECHA_INICIAL"
        Me.FECHA_INICIAL.Name = "FECHA_INICIAL"
        Me.FECHA_INICIAL.Visible = True
        Me.FECHA_INICIAL.VisibleIndex = 5
        Me.FECHA_INICIAL.Width = 87
        '
        'FECHA_FINAL
        '
        Me.FECHA_FINAL.Caption = "Fecha Final"
        Me.FECHA_FINAL.FieldName = "FECHA_FINAL"
        Me.FECHA_FINAL.Name = "FECHA_FINAL"
        Me.FECHA_FINAL.Visible = True
        Me.FECHA_FINAL.VisibleIndex = 6
        Me.FECHA_FINAL.Width = 86
        '
        'DIAS_PAGADOS
        '
        Me.DIAS_PAGADOS.Caption = "Dias Pagados"
        Me.DIAS_PAGADOS.FieldName = "DIAS_PAGADOS"
        Me.DIAS_PAGADOS.Name = "DIAS_PAGADOS"
        Me.DIAS_PAGADOS.Visible = True
        Me.DIAS_PAGADOS.VisibleIndex = 7
        Me.DIAS_PAGADOS.Width = 113
        '
        'ESTADO_PLANILLA
        '
        Me.ESTADO_PLANILLA.Caption = "Estado"
        Me.ESTADO_PLANILLA.FieldName = "ESTADO_PLANILLA"
        Me.ESTADO_PLANILLA.Name = "ESTADO_PLANILLA"
        Me.ESTADO_PLANILLA.Visible = True
        Me.ESTADO_PLANILLA.VisibleIndex = 8
        Me.ESTADO_PLANILLA.Width = 58
        '
        'PLADOCUMENTOBindingSource
        '
        Me.PLADOCUMENTOBindingSource.DataSource = Me.Ds_Produccion
        Me.PLADOCUMENTOBindingSource.Position = 0
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.CheckEdit1)
        Me.GroupControl1.Controls.Add(Me.Label2)
        Me.GroupControl1.Controls.Add(Me.Label1)
        Me.GroupControl1.Controls.Add(Me.TextEdit2)
        Me.GroupControl1.Controls.Add(Me.TextEdit1)
        Me.GroupControl1.Location = New System.Drawing.Point(41, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(231, 142)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "GroupControl1"
        '
        'CheckEdit1
        '
        Me.CheckEdit1.Location = New System.Drawing.Point(76, 99)
        Me.CheckEdit1.Name = "CheckEdit1"
        Me.CheckEdit1.Properties.Caption = "CheckEdit1"
        Me.CheckEdit1.Size = New System.Drawing.Size(75, 19)
        Me.CheckEdit1.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(22, 68)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(38, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Label2"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(22, 38)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(38, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Label1"
        '
        'TextEdit2
        '
        Me.TextEdit2.Location = New System.Drawing.Point(76, 61)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Size = New System.Drawing.Size(100, 20)
        Me.TextEdit2.TabIndex = 1
        '
        'TextEdit1
        '
        Me.TextEdit1.Location = New System.Drawing.Point(76, 35)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Size = New System.Drawing.Size(100, 20)
        Me.TextEdit1.TabIndex = 0
        '
        'Ds_Produccion
        '
        Me.Ds_Produccion.DataSetName = "ds_Produccion"
        Me.Ds_Produccion.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'frmTreeList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(845, 258)
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Name = "frmTreeList"
        Me.Text = "frmTreeList"
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.TreeList1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PLADOCUMENTOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Ds_Produccion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents TreeList1 As DevExpress.XtraTreeList.TreeList
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ANIO_PERIODO As DevExpress.XtraTreeList.Columns.TreeListColumn
    Friend WithEvents MES_PERIODO As DevExpress.XtraTreeList.Columns.TreeListColumn
    Friend WithEvents CORR_PLANILLA As DevExpress.XtraTreeList.Columns.TreeListColumn
    Friend WithEvents FECHA_PLANILLA As DevExpress.XtraTreeList.Columns.TreeListColumn
    Friend WithEvents CORR_TIPO_PLANILLA As DevExpress.XtraTreeList.Columns.TreeListColumn
    Friend WithEvents FECHA_INICIAL As DevExpress.XtraTreeList.Columns.TreeListColumn
    Friend WithEvents FECHA_FINAL As DevExpress.XtraTreeList.Columns.TreeListColumn
    Friend WithEvents DIAS_PAGADOS As DevExpress.XtraTreeList.Columns.TreeListColumn
    Friend WithEvents ESTADO_PLANILLA As DevExpress.XtraTreeList.Columns.TreeListColumn
    Friend WithEvents PLADOCUMENTOBindingSource As BindingSource
    Friend WithEvents Ds_Produccion As ds_Produccion
End Class
