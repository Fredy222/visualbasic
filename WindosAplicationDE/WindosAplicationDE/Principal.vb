﻿Public Class Principal
    Private Sub Principal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'ECOFFEETECH01DataSet.V_CONSULTA8' Puede moverla o quitarla según sea necesario.
        Me.V_CONSULTA8TableAdapter.Fill(Me.ECOFFEETECH01DataSet.V_CONSULTA8)

    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        GridControl1.ShowPrintPreview()
    End Sub

    Private Sub ExportarAExcelToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExportarAExcelToolStripMenuItem.Click
        If sfdRuta.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then
            GridControl1.ExportToXls(sfdRuta.FileName)
        End If
    End Sub
End Class