﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Principal
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Principal))
        Me.VCONSULTA8BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ECOFFEETECH01DataSet = New WindosAplicationDE.ECOFFEETECH01DataSet()
        Me.V_CONSULTA8TableAdapter = New WindosAplicationDE.ECOFFEETECH01DataSetTableAdapters.V_CONSULTA8TableAdapter()
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colNOMBRE_EMPLEADO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSALARIO_MENSUAL = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCORR_PUESTO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCORR_EMPRESA = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.cmMenuGrid = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ExportarAExcelToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.sfdRuta = New System.Windows.Forms.SaveFileDialog()
        CType(Me.VCONSULTA8BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ECOFFEETECH01DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmMenuGrid.SuspendLayout()
        Me.SuspendLayout()
        '
        'VCONSULTA8BindingSource
        '
        Me.VCONSULTA8BindingSource.DataMember = "V_CONSULTA8"
        Me.VCONSULTA8BindingSource.DataSource = Me.ECOFFEETECH01DataSet
        '
        'ECOFFEETECH01DataSet
        '
        Me.ECOFFEETECH01DataSet.DataSetName = "ECOFFEETECH01DataSet"
        Me.ECOFFEETECH01DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'V_CONSULTA8TableAdapter
        '
        Me.V_CONSULTA8TableAdapter.ClearBeforeFill = True
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Horizontal = False
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SimpleButton1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.GridControl1)
        Me.SplitContainerControl1.Size = New System.Drawing.Size(460, 300)
        Me.SplitContainerControl1.SplitterPosition = 104
        Me.SplitContainerControl1.TabIndex = 1
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.AppearancePressed.Image = CType(resources.GetObject("SimpleButton1.AppearancePressed.Image"), System.Drawing.Image)
        Me.SimpleButton1.AppearancePressed.Options.UseImage = True
        Me.SimpleButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.SimpleButton1.ImageOptions.Image = CType(resources.GetObject("SimpleButton1.ImageOptions.Image"), System.Drawing.Image)
        Me.SimpleButton1.Location = New System.Drawing.Point(3, 12)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(132, 56)
        Me.SimpleButton1.TabIndex = 0
        Me.SimpleButton1.Text = "Print"
        '
        'GridControl1
        '
        Me.GridControl1.ContextMenuStrip = Me.cmMenuGrid
        Me.GridControl1.DataMember = Nothing
        Me.GridControl1.DataSource = Me.VCONSULTA8BindingSource
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Enabled = False
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.EndEdit.Enabled = False
        Me.GridControl1.EmbeddedNavigator.Buttons.First.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Remove.Visible = False
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.LookAndFeel.SkinMaskColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.GridControl1.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(460, 191)
        Me.GridControl1.TabIndex = 1
        Me.GridControl1.UseEmbeddedNavigator = True
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.Gray
        Me.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = True
        Me.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.Silver
        Me.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = True
        Me.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.Silver
        Me.GridView1.Appearance.SelectedRow.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.GridView1.Appearance.SelectedRow.Options.UseBackColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colNOMBRE_EMPLEADO, Me.colSALARIO_MENSUAL, Me.colCORR_PUESTO, Me.colCORR_EMPRESA})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.GroupPanelText = "Agrupo Columnas aqui"
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colNOMBRE_EMPLEADO, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colNOMBRE_EMPLEADO
        '
        Me.colNOMBRE_EMPLEADO.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.colNOMBRE_EMPLEADO.AppearanceCell.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.colNOMBRE_EMPLEADO.AppearanceCell.BorderColor = System.Drawing.Color.Black
        Me.colNOMBRE_EMPLEADO.AppearanceCell.Options.UseBackColor = True
        Me.colNOMBRE_EMPLEADO.AppearanceCell.Options.UseBorderColor = True
        Me.colNOMBRE_EMPLEADO.Caption = "Empleado"
        Me.colNOMBRE_EMPLEADO.FieldName = "NOMBRE_EMPLEADO"
        Me.colNOMBRE_EMPLEADO.Name = "colNOMBRE_EMPLEADO"
        Me.colNOMBRE_EMPLEADO.OptionsEditForm.Caption = "NOMBRE EMPLEADO:"
        Me.colNOMBRE_EMPLEADO.OptionsEditForm.Visible = DevExpress.Utils.DefaultBoolean.[True]
        Me.colNOMBRE_EMPLEADO.Visible = True
        Me.colNOMBRE_EMPLEADO.VisibleIndex = 0
        '
        'colSALARIO_MENSUAL
        '
        Me.colSALARIO_MENSUAL.Caption = "Salario Mensual"
        Me.colSALARIO_MENSUAL.FieldName = "SALARIO_MENSUAL"
        Me.colSALARIO_MENSUAL.Name = "colSALARIO_MENSUAL"
        Me.colSALARIO_MENSUAL.OptionsEditForm.Caption = "SALARIO MENSUAL:"
        Me.colSALARIO_MENSUAL.OptionsEditForm.Visible = DevExpress.Utils.DefaultBoolean.[True]
        Me.colSALARIO_MENSUAL.Visible = True
        Me.colSALARIO_MENSUAL.VisibleIndex = 1
        '
        'colCORR_PUESTO
        '
        Me.colCORR_PUESTO.FieldName = "CORR_PUESTO"
        Me.colCORR_PUESTO.Name = "colCORR_PUESTO"
        Me.colCORR_PUESTO.OptionsEditForm.Visible = DevExpress.Utils.DefaultBoolean.[False]
        '
        'colCORR_EMPRESA
        '
        Me.colCORR_EMPRESA.FieldName = "CORR_EMPRESA"
        Me.colCORR_EMPRESA.Name = "colCORR_EMPRESA"
        Me.colCORR_EMPRESA.OptionsEditForm.Visible = DevExpress.Utils.DefaultBoolean.[False]
        '
        'cmMenuGrid
        '
        Me.cmMenuGrid.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExportarAExcelToolStripMenuItem})
        Me.cmMenuGrid.Name = "cmMenuGrid"
        Me.cmMenuGrid.Size = New System.Drawing.Size(181, 48)
        '
        'ExportarAExcelToolStripMenuItem
        '
        Me.ExportarAExcelToolStripMenuItem.Name = "ExportarAExcelToolStripMenuItem"
        Me.ExportarAExcelToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.ExportarAExcelToolStripMenuItem.Text = "Exportar a Excel"
        '
        'sfdRuta
        '
        Me.sfdRuta.Filter = "Archivo Excel | *.xls"
        '
        'Principal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(460, 300)
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Name = "Principal"
        Me.ShowIcon = False
        Me.Text = "Principal"
        CType(Me.VCONSULTA8BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ECOFFEETECH01DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmMenuGrid.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ECOFFEETECH01DataSet As ECOFFEETECH01DataSet
    Friend WithEvents VCONSULTA8BindingSource As BindingSource
    Friend WithEvents V_CONSULTA8TableAdapter As ECOFFEETECH01DataSetTableAdapters.V_CONSULTA8TableAdapter
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colNOMBRE_EMPLEADO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSALARIO_MENSUAL As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCORR_PUESTO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCORR_EMPRESA As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents cmMenuGrid As ContextMenuStrip
    Friend WithEvents ExportarAExcelToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents sfdRuta As SaveFileDialog
End Class
